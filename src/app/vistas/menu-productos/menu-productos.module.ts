import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CategoriasComponent} from './categorias/categorias.component';
import {RouterModule, Routes} from '@angular/router';
import {PrivateContentGuard} from '../../guards/private-content.guard';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {CrearEditarCategoriaComponent} from './crear-editar-categoria/crear-editar-categoria.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {ProductosComponent} from './productos/productos.component';
import { CrearEditarProductoComponent } from './crear-editar-producto/crear-editar-producto.component';
import {MatSelectModule} from '@angular/material/select';

const routes: Routes = [
  {
    path: '',
    component: CategoriasComponent,
    canActivate: [PrivateContentGuard]
  },
  {
    path: ':categoriaId',
    component: ProductosComponent,
    canActivate: [PrivateContentGuard]
  }
];

@NgModule({
  declarations: [CategoriasComponent, CrearEditarCategoriaComponent, ProductosComponent, CrearEditarProductoComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatToolbarModule,
        MatButtonModule,
        MatIconModule,
        MatCardModule,
        FlexLayoutModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatDialogModule,
        MatSnackBarModule,
        ReactiveFormsModule,
        MatProgressBarModule,
        MatSelectModule
    ]
})
export class MenuProductosModule {
}
