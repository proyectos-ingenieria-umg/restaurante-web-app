import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {GenericDialogData, ProductoModel} from '../../../models/models';
import {ProductoService} from '../../../services/producto.service';
import {UtilidadesService} from '../../../services/utilidades.service';
import {CrearEditarProductoComponent} from '../crear-editar-producto/crear-editar-producto.component';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  private unsubscribeAll = new Subject<boolean>();
  private categoriaId = 0;

  searchInput = '';
  mostrarBarraProgreso = false;
  productos: ProductoModel[] = [];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private productoService: ProductoService,
    private utilidadesService: UtilidadesService,
    private dialog: MatDialog,
  ) {
  }

  goToHome(): void {
    this.router.navigate(['home']);
  }

  goToCategorias(): void {
    this.router.navigate(['menu/categorias']);
  }

  buscar(): void {

  }

  actualizarProducto(theProducto: ProductoModel): void {
    const dialogRef = this.dialog
      .open<CrearEditarProductoComponent, GenericDialogData, ProductoModel>(CrearEditarProductoComponent, {
        width: '500px',
        data: {
          operationType: 'EDIT',
          payload: {
            producto: theProducto
          }
        }
      });

    dialogRef.afterClosed().subscribe(productoActualizado => {
      if (productoActualizado) {
        productoActualizado.id = theProducto.id;
        productoActualizado.categoriaId = this.categoriaId;
        this.mostrarBarraProgreso = true;

        this.productoService
          .actualizarProducto(productoActualizado)
          .then((response) => {
            if (response) {
              this.utilidadesService.showSuccessSnackBar('Producto actualizado exitosamente!');
              theProducto.precio = productoActualizado.precio;
              theProducto.productoDescripcion = productoActualizado.productoDescripcion;
              theProducto.productoNombre = productoActualizado.productoNombre;
            }
          })
          .catch(err => {
            console.error(err);
            this.utilidadesService.showErrorSnackBar('Error al actualizar producto');
          })
          .finally(() => {
            this.mostrarBarraProgreso = false;
          });
      }
    });
  }

  crearProducto(): void {
    const dialogRef = this.dialog
      .open<CrearEditarProductoComponent, GenericDialogData, ProductoModel>(CrearEditarProductoComponent, {
        width: '500px',
        data: {
          operationType: 'ADD',
          payload: {
            categoriaId: this.categoriaId
          }
        }
      });

    dialogRef.afterClosed().subscribe(nuevoProducto => {
      if (nuevoProducto) {
        nuevoProducto.categoriaId = this.categoriaId;

        this.mostrarBarraProgreso = true;

        this.productoService
          .crearProducto(nuevoProducto)
          .then(id => {
            if (id > 0) {
              nuevoProducto.id = id;
              this.utilidadesService.showSuccessSnackBar('Producto agregado exitosamente!');
              this.productos.push(nuevoProducto);
            }
          })
          .catch(err => {
            console.error(err);
            this.utilidadesService.showErrorSnackBar('Error al crear nuevo producto');
          })
          .finally(() => {
            this.mostrarBarraProgreso = false;
          });
      }
    });
  }

  private obtenerProductosPorCategoria(categoriaId: number): void {
    this.mostrarBarraProgreso = true;
    this.productoService
      .obtenerProductosPorCategoria(categoriaId)
      .then(productos => {
        this.productos = productos;
      })
      .catch(err => {
        console.error(err);
        this.utilidadesService.showErrorSnackBar('Error al obtener los productos');
      })
      .finally(() => {
        this.mostrarBarraProgreso = false;
      });
  }

  ngOnInit(): void {
    this.activatedRoute
      .params
      .pipe(takeUntil(this.unsubscribeAll))
      .subscribe(params => {
        if (params.categoriaId) {
          this.categoriaId = parseInt(params.categoriaId, 10);
          this.obtenerProductosPorCategoria(this.categoriaId);
        }
      });
  }

}
