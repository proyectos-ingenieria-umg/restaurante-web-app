import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ControlMesasComponent} from './control-mesas.component';
import {RouterModule, Routes} from '@angular/router';
import {PrivateContentGuard} from '../../guards/private-content.guard';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import { CrearEditarMesaComponent } from './crear-editar-mesa/crear-editar-mesa.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';

const routes: Routes = [
  {
    path: '',
    component: ControlMesasComponent,
    canActivate: [PrivateContentGuard]
  }
];

@NgModule({
  declarations: [ControlMesasComponent, CrearEditarMesaComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatSnackBarModule,
        MatToolbarModule,
        MatButtonModule,
        FlexLayoutModule,
        MatIconModule,
        MatInputModule,
        MatFormFieldModule,
        ReactiveFormsModule,
        FormsModule,
        MatCardModule,
        MatDialogModule,
        MatProgressBarModule
    ]
})
export class ControlMesasModule {
}
