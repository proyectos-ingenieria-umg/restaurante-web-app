import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {take} from 'rxjs/operators';

@Injectable()
export class AuthContentGuard implements CanActivate {
  
  constructor(public router: Router, private authenticationService: AuthenticationService) {
  }

  /**
   * Esta funcion verifica si puede acceder o no, a una URL dentro de la aplicacion
   * @param next objeto ActivatedRouteSnapshot
   * @param state objeto RouterStateSnapshot
   */
  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const isLogged = await this.authenticationService
      .isLogged
      .pipe(take(1))
      .toPromise();

    if (isLogged) {
      console.error('Usted ya se encuentra logueado');
      this.router.navigate(['/home']);
      return false;
    }

    return true;
  }

}
