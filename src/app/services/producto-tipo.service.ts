import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProductoTipoModel} from '../models/models';
import {environment} from '../../environments/environment';

@Injectable()
export class ProductoTipoService {

  constructor(private readonly http: HttpClient) {
  }

  obtenerProductoTipos(): Promise<ProductoTipoModel[]> {
    const url = `${environment.api}producto-tipo`;
    return this.http.get<ProductoTipoModel[]>(url).toPromise();
  }


}
