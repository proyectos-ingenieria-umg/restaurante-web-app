import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthenticationService} from './authentication.service';
import {MesaService} from './mesa.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {UtilidadesService} from './utilidades.service';
import {CategoriaService} from './categoria.service';
import {ProductoService} from './producto.service';
import {ProductoTipoService} from './producto-tipo.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatSnackBarModule
  ],
  providers: [
    AuthenticationService,
    UtilidadesService,
    MesaService,
    CategoriaService,
    ProductoService,
    ProductoTipoService
  ]
})
export class ServicesModule {
}
